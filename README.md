Web App in Python
-----------------

Basierend auf [Erstellen einer Webanwendung mit Flask in Python 3](https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3-de).


    mkdir py-web-app
    cd py-web-app

    cat <<EOF >hello.py
    from flask import Flask
    app = Flask(__name__)
    @app.route('/')
    def hello():
        return 'Hello, World!'    
    EOF

    pip install flask

    export FLASK_APP=hello
    export FLASK_ENV=development
    export PATH=$PATH:~/.local/bin/
    flask run

    # Testen in neuem Terminal
    curl localhost:5000
